'use strict';


$(document).ready(function () {

	$('#card__slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		afterChange: function() {

		}
	});
	$('.zoom').zoom();
	$(".card__lsider__wrap").on("click", function (e){
		e.stopPropagation();
		e.preventDefault();

		var index = $(this).attr("data-slide");

		if (index != undefined) {
			$("#card__slider").slick("slickGoTo", index);
		}
	} );

	$("#card__slider__nav .slick-slide:first").on("click", function (e) {
		e.stopPropagation();
		e.preventDefault();
	});




	$('.view__slider').slick({
		dots: false,
		infinite: false,
		speed: 300,
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: false,
	});
	$(".view  .arrow_right").on('click', function () {
		var maxCount = $(".view__slider").find(".slick-slide").length - 6;
		
		$(".view .arrow_left").removeClass("slick-disabled");

		if ( $(".view__slider").slick("slickCurrentSlide") >= maxCount ) {
			$(".view  .arrow_right").addClass("slick-disabled");
		} else {
			$(".view  .arrow_right").removeClass("slick-disabled");
		}

		$(".view__slider").slick("slickNext");
	});
	$(".view .arrow_left").on('click', function () {
		$(".view .arrow_right").removeClass("slick-disabled");

		if ( $(".view__slider").slick("slickCurrentSlide") <= 1 ) {
			$(".view .arrow_left").addClass("slick-disabled");
		} else {
			$(".view .arrow_left").removeClass("slick-disabled");
		}

		$(".view__slider").slick("slickPrev");
	});
	
	mobileOnlySlider();
	function mobileOnlySlider () {
		$(".card__https__slider").each(function(index, element) {
			$(element).slick({
				settings: "unslick",
				speed: 300,
				infinite: false,
				dots: false,
				arrows: true,
				slidesToShow: 4,
				slidesToScroll: 1,
				nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
				prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
				responsive: [
					{
						breakpoint: 991,
						settings: {
							
							speed: 300,
							infinite: false,
							dots: false,
							arrows: true,
							slidesToShow: 1,
							slidesToScroll: 1,
							nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
							prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
						}
					}
				]
			});
		});






	}
	

	// init sliders
	(function () {
		// для файлов
		if($(window).width() < 991) {
			if(!$('.card__https__slider').hasClass('slick-initialized')){
				mobileOnlySlider();
			}
			
		} else {
			if($('.card__https__slider').hasClass('slick-initialized')) {
				$(".card__https__slider").each(function(index, element) {
					//$(element).slick('unslick');

					//$(element).find(".slick-slide").css("width", "25%");
				});
			}
		}

		// для модификаций
		if($(window).width() < 991) {
			if(!$('.modification__slider').hasClass('slick-initialized')){
				mobileOnlySliderMod();
			}
			
		} else {
			if($('.modification__slider').hasClass('slick-initialized')) {
				$(".modification__slider").each(function(index, element) {
					//$(element).slick('unslick');

					//$(element).find(".slick-slide").css("width", "25%");
				});
			}
		}
	})();

	$(window).resize(function(e){

		// А работает ли это вообще? О_о
		// Ожидание: мобилка - показать слайдер. декстоп - не слайдер, но блок есть
		// Реализация: ХЗ -_-

		// для файлов
		if($(window).width() < 991) {
			if(!$('.card__https__slider').hasClass('slick-initialized')){
				mobileOnlySlider();
			}
			
		} else {
			if($('.card__https__slider').hasClass('slick-initialized')) {
				$(".card__https__slider").each(function(index, element) {
					//$(element).slick('unslick');

					//$(element).find(".slick-slide").css("width", "25%");
				});
			}
		}


		// для модификаций
		if($(window).width() < 991) {
			if(!$('.modification__slider').hasClass('slick-initialized')){
				mobileOnlySliderMod();
			}
			
		} else {
			if($('.modification__slider').hasClass('slick-initialized')) {
				$(".modification__slider").each(function(index, element) {
					//$(element).slick('slickRemove');

					//$(element).find(".slick-slide").css("width", "25%");
				});
			}
		}
	});

	$('#carouselModificationInterval').carousel()

	// слайдре для модификация
	function mobileOnlySliderMod () {
		$(".modification__slider").each(function(index, element) {
			$(element).slick({
				settings: "unslick",
				responsive: [
					{
						breakpoint: 991,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2,
							speed: 300,
							infinite: false,
							dots: false,
							arrows: true,
							slidesToShow: 1,
							slidesToScroll: 1,
							nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
							prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
						}
					},
						{
						breakpoint: 480,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							speed: 300,
							infinite: false,
							dots: false,
							arrows: true,
							slidesToShow: 1,
							slidesToScroll: 1,
							nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
							prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
						}
					}
					// You can unslick at a given breakpoint now by adding:
					// settings: "unslick"
					// instead of a settings object
				]
			});
		});
	}

	// слайдер для card__project под мобилки
	$(".card__project__slider").each(function (index, element) {
		$(element).slick({
			responsive: [
				{
					breakpoint: 991,
					settings: {
						speed: 300,
						infinite: false,
						dots: false,
						arrows: true,
						slidesToShow: 1,
						slidesToScroll: 1,
						nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
						prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
					}
				}
			]
		});
	});


	// слайдер для copy блока под мобилки
	$(".copy__slider").each(function (index, element) {
		$(element).slick({
			slidesToShow: 2,
			slidesToScroll: 2,
			speed: 300,
			infinite: false,
			dots: false,
			arrows: true,
			slidesToShow: 4,
			slidesToScroll: 1,
			nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
			prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
			responsive: [
				{
					breakpoint: 991,
					settings: {
						settings: "unslick",
						slidesToShow: 2,
						slidesToScroll: 2,
						speed: 300,
						infinite: false,
						dots: false,
						arrows: true,
						slidesToShow: 1,
						slidesToScroll: 1,
						nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
						prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
					}
				},
					{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						speed: 300,
						infinite: false,
						dots: false,
						arrows: true,
						slidesToShow: 1,
						slidesToScroll: 1,
						nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
						prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
					}
				}
				// You can unslick at a given breakpoint now by adding:
				// settings: "unslick"
				// instead of a settings object
			]
		});
	});





	// ылайдер картинок
	$(".facts__slider").slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				speed: 300,
				infinite: false,
				dots: false,
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
				prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
		});



	// слайдер для copy блока под мобилки
	$(".otsivi__slider").each(function (index, element) {
		$(element).slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			speed: 300,
			infinite: false,
			dots: false,
			arrows: true,
			nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
			prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
			responsive: [
				{
					breakpoint: 991,
					settings: {
						settings: "unslick",
						speed: 300,
						infinite: false,
						dots: false,
						arrows: true,
						slidesToShow: 1,
						slidesToScroll: 1,
						nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
						prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
					}
				},
					{
					breakpoint: 480,
					settings: {
						speed: 300,
						infinite: false,
						dots: false,
						arrows: true,
						slidesToShow: 1,
						slidesToScroll: 1,
						nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
						prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
					}
				}
				// You can unslick at a given breakpoint now by adding:
				// settings: "unslick"
				// instead of a settings object
			]
		});
	});




	$(".projects__slider").each(function(index, element) {
		$(element).slick({
			speed: 300,
			infinite: false,
			dots: false,
			arrows: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
			prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
		});
	});



	$(".payment__slider").each(function(index, element) {
		$(element).slick({
			speed: 300,
			infinite: false,
			dots: false,
			arrows: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
			prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
		});
	});


	$(".blogpost__slider").each(function(index, element) {
		$(element).slick({
			lazyLoad: 'ondemand',
			speed: 300,
			infinite: false,
			dots: false,
			arrows: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
			prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
			beforeChange: function (event, slick, currentSlide, nextSlide) {
				console.log(123123);
			}
		});

		$(element).on('beforeChange', function(event, slick, currentSlide, nextSlide){
			$(element).parent().find(".blogpost__slider__count__first").html(nextSlide + 1);
		});
        
	});


	$(".recommendation").each(function(index, element) {
		$(element).slick({
			lazyLoad: 'ondemand',
			speed: 300,
			infinite: false,
			dots: false,
			arrows: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
			prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
			responsive: [
				{
					breakpoint: 991,
					settings: {
						settings: "unslick",
						speed: 300,
						infinite: false,
						dots: false,
						arrows: true,
						slidesToShow: 1,
						slidesToScroll: 1,
						nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
						prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
					}
				},
					{
					breakpoint: 480,
					settings: {
						speed: 300,
						infinite: false,
						dots: false,
						arrows: true,
						slidesToShow: 1,
						slidesToScroll: 1,
						nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
						prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
					}
				}
			]
		});
	});



$(".main__slider").each(function(index, element) {
		$(element).slick({
			lazyLoad: 'ondemand',
			speed: 300,
			infinite: false,
			dots: true,
			arrows: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 991,
					settings: {
						settings: "unslick",
						speed: 300,
						infinite: false,
						dots: true,
						arrows: false,
						slidesToShow: 1,
						slidesToScroll: 1,
					}
				}
			]
		});
	});
$(".main__second__slider").each(function(index, element) {
	$(element).slick({
		lazyLoad: 'ondemand',
		speed: 300,
		infinite: false,
		dots: false,
		arrows: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
		prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
	});
});




$(".main__fourth__slider").each(function(index, element) {
	$(element).slick({
		lazyLoad: 'ondemand',
		speed: 300,
		infinite: false,
		dots: false,
		arrows: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
		prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
	});
});



$(".main__sixth__slider").each(function(index, element) {
	$(element).slick({
		lazyLoad: 'ondemand',
		speed: 300,
		infinite: false,
		dots: false,
		arrows: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
		prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
	});
});



$(".main__seventh__slider").each(function(index, element) {
	$(element).slick({
		lazyLoad: 'ondemand',
		speed: 300,
		infinite: false,
		dots: false,
		arrows: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
		prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
	});
});



$(".project__slider").each(function(index, element) {
	$(element).slick({
		lazyLoad: 'ondemand',
		speed: 300,
		infinite: false,
		dots: true,
		arrows: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		nextArrow: '<div class="arrow arrow_right"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
		prevArrow: '<div class="arrow arrow_left"><svg xmlns="http://www.w3.org/2000/svg" width="10.607" height="18.385" fill="#707070" viewBox="0 0 10.607 18.385"><g data-name="Symbol 110" transform="translate(-43.807)"><g id="" data-name="Group 5899" transform="translate(181 495.192) rotate(180)" opacity="0.2"><rect id="" data-name="Rectangle 5555" width="2" height="13" transform="translate(128 487.414) rotate(-135)"></rect><rect id="" data-name="Rectangle 5554" width="2" height="2" transform="translate(128 484.586) rotate(45)"></rect><rect id="" data-name="Rectangle 5556" width="2" height="13" transform="translate(137.192 493.778) rotate(135)"></rect></g></g></svg></div>',
	});
});





});


