

$(document).ready(function () {

	$( "#slider-range-one" ).slider({
		range: true,
		min: 0,
		max: 500,
		values: [ 0, 500 ],
		slide: function( event, ui ) {
			$("#slider-range-one__min").val( ui.values[ 0 ] );
			$("#slider-range-one__max").val( ui.values[ 1 ] );
		}
	});
	$("#slider-range-one__min").change(function () {
		$("#slider-range-one").slider( 'values',0, $(this).val() );
	})
	$("#slider-range-one__max").change(function () {
		$("#slider-range-one").slider( 'values',1, $(this).val() );
	})


	$( "#slider-range-two" ).slider({
		range: true,
		min: 0,
		max: 500,
		values: [ 0, 500 ],
		slide: function( event, ui ) {
			$("#slider-range-two__min").val( ui.values[ 0 ] );
			$("#slider-range-two__max").val( ui.values[ 1 ] );
		}
	});
	$("#slider-range-two__min").change(function () {
		$("#slider-range-two").slider( 'values',0, $(this).val() );
	})
	$("#slider-range-two__max").change(function () {
		$("#slider-range-two").slider( 'values',1, $(this).val() );
	})



	$( "#slider-range-three" ).slider({
		range: true,
		min: 0,
		max: 500,
		values: [ 0, 500 ],
		slide: function( event, ui ) {
			$("#slider-range-three__min").val( ui.values[ 0 ] );
			$("#slider-range-three__max").val( ui.values[ 1 ] );
		}
	});
	$("#slider-range-three__min").change(function () {
		$("#slider-range-three").slider( 'values',0, $(this).val() );
	})
	$("#slider-range-three__max").change(function () {
		$("#slider-range-three").slider( 'values',1, $(this).val() );
	})


	$( "#slider-range-four" ).slider({
		range: true,
		min: 0,
		max: 500,
		values: [ 0, 500 ],
		slide: function( event, ui ) {
			$("#slider-range-four__min").val( ui.values[ 0 ] );
			$("#slider-range-four__max").val( ui.values[ 1 ] );
		}
	});
	$("#slider-range-four__min").change(function () {
		$("#slider-range-four").slider( 'values',0, $(this).val() );
	})
	$("#slider-range-four__max").change(function () {
		$("#slider-range-four").slider( 'values',1, $(this).val() );
	})




	$( "#slider-range-five" ).slider({
		range: true,
		min: 0,
		max: 500,
		values: [ 0, 500 ],
		slide: function( event, ui ) {
			$("#slider-range-five__min").val( ui.values[ 0 ] );
			$("#slider-range-five__max").val( ui.values[ 1 ] );
		}
	});
	$("#slider-range-five__min").change(function () {
		$("#slider-range-five").slider( 'values',0, $(this).val() );
	})
	$("#slider-range-five__max").change(function () {
		$("#slider-range-five").slider( 'values',1, $(this).val() );
	})
	
	



});

