'use strict';

//= ../node_modules/jquery/dist/jquery.js
//= library/jquery-ui.js

//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/popper.js/dist/umd/popper.js
//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js
//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js

//= library/wow.js

//= library/jquery.event.move.js
//= library/jquery.twentytwenty.js
//= library/slick.js
//= library/jquery.zoom.js


//= components/searchInput.js
//= components/slider_slick.js
//= components/jqueryUIcategory.js
//= components/actionModalVideo.js



$(document).ready(function () {

	$("#beforeafter").twentytwenty();
	

	/* анимация блоков */
	new WOW().init({
		mobile: false
	});


	/* START Открытие меню */
	$(".btn__menu").on("click", function () {
		$(this).toggleClass("active");

		if ( $(this).hasClass("active") ) {
			$(".navigation__content").addClass("active");
			$("body").css("overflow", "hidden");
		} else {
			$(".navigation__content").removeClass("active");
			$("body").css("overflow", "auto");
		}
	});

	$(".btn_header").on("click", function () {
		if( $(".navigation__content").hasClass("active") ) {
			$(".navigation__content").removeClass("active");
			$(".btn__menu").removeClass("active");
			$("#burger").removeClass("active");
			$("body").css("overflow", "auto");
		}
	});
	/* END откртие меню*/

	/* START красиво для hover в navigation */
	$(".navigation ul li a").hover(function () {
		var width = $(this).width();

		var elem = $(this);
		var offset = elem.offset().left - elem.parent().parent().offset().left;
		offset += parseInt( elem.css("padding-left") );

		$(".navigation__line").width( width );
		$(".navigation__line").css("left", offset);
		$(".navigation__line").css("bottom", "-6px");
	}, function () {
		// Забрать красоту! ВСЮ!
		$(".navigation__line").css("bottom", "-500%");
	});
	/* END красиво для hover в navigation */

	if ( $(window).scrollTop() > 0 ) {
        //$("header").addClass("bg-dark");
    }
	$(window).on("scroll", function () {
		if ( $(window).scrollTop() > 0 ) {
			//$("header").addClass("bg-dark");
		}  else {
			//$("header").removeClass("bg-dark");
		}

		/* START кнопка вверх */
		if ( $(window).scrollTop() > 100 ) {
			$(".btn-to-top").addClass("btn-to-top_active");
		} else {
			$(".btn-to-top").removeClass("btn-to-top_active");
		}

	});
	$('.btn-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });
	/* END кнопка вверх */

	$(".requisites__btn").on("click", function () {
		window.print();
	});


	// плавный якорь
	$(".btn_move").on("click", function (event) {
		//отменяем стандартную обработку нажатия по ссылке
		event.preventDefault();
		console.log("dfgsdfg");
		//забираем идентификатор бока с атрибута href
		var id  = $(this).attr('href'),

		//узнаем высоту от начала страницы до блока на который ссылается якорь
			top = $(id).offset().top;
		
		//анимируем переход на расстояние - top за 1500 мс
		$('body,html').animate({scrollTop: top}, 1000);
	});


	$(window).resize(function () {
		filterMob();
	});
	filterMob();
	function filterMob () {
		if ( $(window).width() > 991) {
			$("#collapseMob").collapse("show");
		} else {
			$("#collapseMob").collapse('hide');
		}
	}


	$(".hed").hover(function () {
		$("body").css("overflow", "hidden");
		$("body").css('padding-right', widthScrollWindow());
	}, function () {
		$("body").css("overflow", "auto");
		$("body").css("padding-right", 0);
	});


	function widthScrollWindow() {
		var div = document.createElement('div');

		div.style.overflowY = 'scroll';
		div.style.width = '50px';
		div.style.height = '50px';

		// мы должны вставить элемент в документ, иначе размеры будут равны 0
		document.body.append(div);
		var scrollWidth = div.offsetWidth - div.clientWidth;

		div.remove();

		return scrollWidth;
	}

	
	$( function() {
    	$('[data-toggle="tooltip"]').tooltip();
	} );



	$('#modalCall').on('show.bs.modal', function (e) {
		$(".navigation__content").removeClass("active");
		$(".btn__menu").removeClass("active");
		$("#burger").removeClass("active");
		$("body").css("overflow", "auto");
		console.log("open");
	})




	if ( $("#map").length ) {
		ymaps.ready(function () {

		var myMap = new ymaps.Map('map', {
	       center: [55.89396006885465,37.64423449999994],
	        zoom: 17,
	        controls: []
	    }, {
	        searchControlProvider: 'yandex#search'
	    }),

	    // Создаём макет содержимого.
	    MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
	        '<div style="color: #FFFFFF; font-weight: bold;" class="qwe">$[properties.iconContent]</div>'
	    ),

	    myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
	    	balloonContentHeader: "<span style='font-size: 16px; color: #333333; font-family: Gilroy;'>TRC River Mall</span>",
	        balloonContentBody: "<span style='color:#969696; font-size: 13px;'>Адрес: Москва, ул. Полярная, д. 41, стр. 1</spam>",
	         closeButton: false
	    }, {
	        // Опции.
	        // Необходимо указать данный тип макета.
	        iconLayout: 'default#image',
	        // Своё изображение иконки метки.
	        iconImageHref: 'images/location-marker.png',
	        // Размеры метки.
	        iconImageSize: [34, 42],
	        // Смещение левого верхнего угла иконки относительно
	        // её "ножки" (точки привязки).
	        iconImageOffset: [-17, -38]
	    });

	    myMap.geoObjects
	        .add(myPlacemark);
		});
	}


	$("#carouselModificationInterval").on("slid.bs.carousel", function () {
		var items = $("#carouselModificationInterval").find(".carousel-item");

		for(var i = 0; i < items.length; i++) {
			if ( $(items[i]).hasClass("active") ) {
				if (i == 0) {
					$("#carouselModificationInterval .carousel-control-prev").addClass("false");
					$("#carouselModificationInterval .carousel-control-next").removeClass("false");
				} else if ( i == items.length - 1) {
					$("#carouselModificationInterval .carousel-control-next").addClass("false");
					$("#carouselModificationInterval .carousel-control-prev").removeClass("false");
				} else {
					$("#carouselModificationInterval .carousel-control-next").removeClass("false");
					$("#carouselModificationInterval .carousel-control-prev").removeClass("false");
				}
				return true;
			}
		}
	});
	

});


